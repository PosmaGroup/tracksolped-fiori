/*global location */
sap.ui.define([
		"com/bac/trackpr/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"com/bac/trackpr/model/formatter",
		"sap/ui/model/odata/v2/ODataModel",
		"sap/ui/model/odata/OperationMode",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"sap/ui/core/Fragment",
		"sap/m/MessageToast",
		"sap/ui/core/format/DateFormat",
		"sap/ui/core/IconColor"
	], function (BaseController, JSONModel, formatter, ODataModel, Mode, Filter, FilterOperator, Fragment, MessageToast, DateFormat, IconColor) {
		"use strict";
		var bPaid, bDelivered, bInvoiced, bDeliveredChecked, bInvoicedChecked, bPaidChecked;

		return BaseController.extend("com.bac.trackpr.controller.Detail", {

			formatter: formatter,

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			onInit : function () {
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var oViewModel = new JSONModel({
					busy : false,
					delay : 0,
					lineItemListTitle : this.getResourceBundle().getText("detailLineItemTableHeading")
				});

				this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);
				this.setModel(oViewModel, "detailView");
				this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
				var oDetailsModel = new JSONModel();
				this.getView().setModel(oDetailsModel, "PODetails");
			},
			
			onAfterRendering: function() {
			    var oUserModel = new JSONModel("/services/userapi/currentUser");
			    this.getView().setModel(oUserModel, "userapi");
			},

			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */
			/**
			 * Updates the item count within the line item table's header
			 * @param {object} oEvent an event containing the total number of items in the list
			 * @private
			 */
			onListUpdateFinished : function (oEvent) {
			    if (oEvent.getParameter("reason") === "Refresh") {
			        var iTotalItems = oEvent.getParameter("total"),
					oViewModel = this.getModel("detailView");
					oViewModel.setProperty("/lineItemListTitle", iTotalItems);
			    }
			},
			
			onSelectedTab: function(oEvent) {
			},
			
			onNavBack: function(oEvent) {
                this.getRouter().getTargets().display("master");  
			},
			
			onPressed: function(oEvent) {
			    sap.ui.getCore().setModel(true, "selectedTab");
			    var sSelectedItemId = oEvent.getSource().getId();
			    var aItems = this.byId(Fragment.createId("deliveredPOItems", "poStatusDelivered")).getItems();
			    var sServiceUrl = this.getView().getModel("trackPO").sServiceUrl;
                var oModel = new ODataModel(sServiceUrl);
                var sPath;
			    	
			    for (var i = 0; i < aItems.length; i++) {
                    if (aItems[i].getId() === sSelectedItemId) {
                        sPath = aItems[i].getBindingContextPath();
                        var mPOItems = {};
                        mPOItems = oEvent.getSource().getBindingContext("Delivered").getObject(sPath);
                        break;   
                    }
			    }
			    
			    sPath = "/POItemDetailDatas(PONumber='" + mPOItems.PONumber + "',ItemNumber='" + mPOItems.ItemNumber + "')";
				var that = this;
				
				oModel.attachBatchRequestSent( function () {
					this.openBusyIndicator();
				}, this);
				
			    oModel.read(sPath, {
					urlParameters: {
                        "$expand": "POItemDocFlows"
                    },
					success : function(oData, oResponse) {
					    aItems = [];
					    for (i = 0; i < oData.POItemDocFlows.results.length; i++) {
                            if (oData.POItemDocFlows.results[i].DocumentType === "1") {
                                aItems.push(oData.POItemDocFlows.results[i]);
                            }       
					    }
					    
					    if (aItems.length === 0) {
					        MessageToast.show(that.getResourceBundle().getText("noDeliveredItems"));
					    } else {
					        var oJSONModel = new JSONModel();
    			            oJSONModel.setData(aItems);
                            sap.ui.getCore().setModel(oJSONModel, "deliveredDetails");
    				
            			    that.getRouter().navTo("delivered", {
            			        id: mPOItems.PONumber,
            			        item: mPOItems.ItemNumber
            			    }, true);
					    } 
					},
					error : function(oError) {
					    MessageToast.show(that.getResourceBundle().getText("noDeliveredItems"));
					}
				});
			},
			
			onPressedInvoice: function(oEvent) {
			    sap.ui.getCore().setModel(true, "selectedTab");
			    var sSelectedItemId = oEvent.getSource().getId();
			    var aItems = this.byId(Fragment.createId("invoicedPOItems", "poStatusInvoiced")).getItems();
			    var sServiceUrl = this.getView().getModel("trackPO").sServiceUrl;
                var oModel = new ODataModel(sServiceUrl);
                var sPath;
			    	
			    for (var i = 0; i < aItems.length; i++) {
                    if (aItems[i].getId() === sSelectedItemId) {
                        sPath = aItems[i].getBindingContextPath();
                        var mPOItems = {};
                        mPOItems = oEvent.getSource().getBindingContext("Invoiced").getObject(sPath);
                        break;   
                    }
			    }
			    
			    sPath = "/POItemDetailDatas(PONumber='" + mPOItems.PONumber + "',ItemNumber='" + mPOItems.ItemNumber + "')";
				var that = this;
				
				oModel.attachBatchRequestSent( function () {
					this.openBusyIndicator();
				}, this);
				
			    oModel.read(sPath, {
					urlParameters: {
                        "$expand": "POItemDocFlows"
                    },
					success : function(oData, oResponse) {
					    aItems = [];
					    for (i = 0; i < oData.POItemDocFlows.results.length; i++) {
                            if (oData.POItemDocFlows.results[i].DocumentType === "2") {
                                aItems.push(oData.POItemDocFlows.results[i]);
                            }       
					    }
					    
					   if (aItems.length === 0) {
					        MessageToast.show(that.getResourceBundle().getText("noDeliveredItems"));
					   } else {
					        var oJSONModel = new JSONModel();
    			            oJSONModel.setData(aItems);
                            sap.ui.getCore().setModel(oJSONModel, "invoiceDetails");
    				
            			    that.getRouter().navTo("invoiced", {
            			        id: mPOItems.PONumber,
            			        item: mPOItems.ItemNumber
            			    });
					    } 
					},
					error : function(oError) {
					}
				});
			},
			
			onPressedPaid: function(oEvent) {
			    sap.ui.getCore().setModel(true, "selectedTab");
			    var sSelectedItemId = oEvent.getSource().getId();
			    var aItems = this.byId(Fragment.createId("paidPOItems", "poStatusPaid")).getItems();
			    var sServiceUrl = this.getView().getModel().sServiceUrl;
                var oModel = new ODataModel(sServiceUrl);
                var sPath;
			    	
			    for (var i = 0; i < aItems.length; i++) {
                    if (aItems[i].getId() === sSelectedItemId) {
                        sPath = aItems[i].getBindingContextPath();
                        var mPOItems = {};
                        mPOItems = oEvent.getSource().getBindingContext("Paid").getObject(sPath);
                        break;   
                    }
			    }
			    
			    var aFilter = new Filter("Ebeln", FilterOperator.EQ, mPOItems.PONumber);
				sPath = "/PaidDocumentsSet";
				var that = this;
				
				oModel.attachBatchRequestSent( function () {
					this.openBusyIndicator();
				}, this);
				
				oModel.read(sPath, {
					filters : [aFilter],
					success : function(oData, oResponse) {
                        for (i = 0; i < oData.results.length; i++) {
                            if (oData.results[i].Augdt) {
                                oData.results[i].Augdt = that._formatLongDate(oData.results[i].Augdt);
                            }
                        }
                        
                        var oJSONModel = new JSONModel();
                        oJSONModel.setData(oData.results);
                        sap.ui.getCore().setModel(oJSONModel, "Paid");
			    
                        that.getRouter().navTo("paid", {
                            id: mPOItems.PONumber
                        });
					},
					error : function(oError) {
					}
				});
			},
			
			handlePRApprovers: function(oEvent) {
			    sap.ui.getCore().setModel(true, "selectedTab");
			    var sSelectedItemId = oEvent.getSource().getId();
			    var aItems = this.byId(Fragment.createId("fragmentPRApprovers", "PRList")).getItems();
			    	
			    for (var i = 0; i < aItems.length; i++) {
                    if (aItems[i].getId() === sSelectedItemId) {
                        var sPath = aItems[i].getBindingContextPath();
                        var mPRItems = {};
                        mPRItems = oEvent.getSource().getBindingContext().getObject(sPath);
                        break;   
                    }
			    }

			    this.openBusyIndicator();
			    
			    this.getRouter().navTo("prapprovers", {
                    id: mPRItems.Banfn,
                    item: mPRItems.Bnfpo
                }, true);
			},
			
			handlePOApprovers: function(oEvent) {
			    sap.ui.getCore().setModel(true, "selectedTab");
			    var sSelectedItemId = oEvent.getSource().getId();
			    var aItems = this.byId(Fragment.createId("fragmentPOApprovers", "POList")).getItems();
			    	
			    for (var i = 0; i < aItems.length; i++) {
                    if (aItems[i].getId() === sSelectedItemId) {
                        var sPath = aItems[i].getBindingContextPath();
                        var mPOItems = {};
                        mPOItems = oEvent.getSource().getBindingContext("POItems").getObject(sPath);
                        break;   
                    }
			    }
			    
			    this.openBusyIndicator();
			    
			    this.getRouter().navTo("poapprovers", {
                    id: mPOItems.Ebeln
                }, true);
			},
			
			handleLinkPressPR: function(oEvent) {
			    sap.ui.getCore().setModel(true, "selectedTab");
			    var sSelectedItemId = oEvent.getSource().getParent().getId();
			    var aItems = this.byId(Fragment.createId("fragmentPRItems", "lineItemsList")).getItems();
			    var sServiceUrl = this.getView().getModel().sServiceUrl;
                var oModel = new ODataModel(sServiceUrl);
                var sPath;
			    	
			    for (var i = 0; i < aItems.length; i++) {
                    if (aItems[i].getId() === sSelectedItemId) {
                        sPath = aItems[i].getBindingContextPath();
                        var mPRItems = {};
                        mPRItems = this.getView().getModel().getObject(sPath);
                        break;   
                    }
			    }
			    
			    var oFilterBanfn = new Filter("Banfn", FilterOperator.EQ, mPRItems.Banfn);
			    var oFilterBnfpo = new Filter("Bnfpo", FilterOperator.EQ, mPRItems.Bnfpo);
			    var oFilter = new Filter({
			        filters: [oFilterBanfn, oFilterBnfpo], 
			        and:true
			    });
			    
				sPath = "/PRAccountAssignmentSet";
				var that = this;
				
				oModel.attachBatchRequestSent( function () {
					this.openBusyIndicator();
				}, this);
				
				oModel.read(sPath, {
					filters : [oFilter],
					success : function(oData, oResponse) {
					    var oJSONModel = new JSONModel();
			            oJSONModel.setData(oData.results);
                        sap.ui.getCore().setModel(oJSONModel, "PRAcc");
				
        			    that.getRouter().navTo("prAccAssignment", {
        			        id: mPRItems.Banfn,
        			        item: mPRItems.Bnfpo
        			    }, true);
					},
					error : function(oError) {
					}
				});
			},
			
			handleLinkPressPO: function(oEvent) {
			    sap.ui.getCore().setModel(true, "selectedTab");
			    var sSelectedItemId = oEvent.getSource().getParent().getId();
			    var aItems = this.byId(Fragment.createId("fragmentPOItems", "poItemsList")).getItems();
			    	
			    for (var i = 0; i < aItems.length; i++) {
                    if (aItems[i].getId() === sSelectedItemId) {
                        var sPath = aItems[i].getBindingContextPath();
                        var mPOItems = {};
                        mPOItems = this.getView().getModel("PODetails").getObject(sPath);
                        break;   
                    }
			    }
			    
				var oJSONModel = new JSONModel();
				aItems = mPOItems.POAccAssignment.results;
	            oJSONModel.setData(aItems);
                sap.ui.getCore().setModel(oJSONModel, "POAcc");
				this.openBusyIndicator();
				
			    this.getRouter().navTo("poAccAssignment", {
			        id: mPOItems.Ebeln,
			        item: mPOItems.Ebelp
			    }, true);
			},

			/* =========================================================== */
			/* begin: internal methods                                     */
			/* =========================================================== */

			/**
			 * Binds the view to the object path and expands the aggregated line items.
			 * @function
			 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
			 * @private
			 */
			_onObjectMatched : function (oEvent) {
				var sObjectId =  oEvent.getParameter("arguments").objectId;
				var oPOItems = this.byId(Fragment.createId("fragmentPOItems", "poItemsList"));
				var oApprovers = this.byId(Fragment.createId("fragmentPOApprovers", "POList"));
				var oDelivered = this.byId(Fragment.createId("deliveredPOItems", "poStatusDelivered"));
				var oScanned = this.byId(Fragment.createId("fragmentInvoicedPO", "InvoicedPOList"));
				var oInvoiced = this.byId(Fragment.createId("invoicedPOItems", "poStatusInvoiced"));
				var oPaid = this.byId(Fragment.createId("paidPOItems", "poStatusPaid"));
				
				if (oPOItems.getModel("POItems")) {
				    oPOItems.getModel("POItems").setData();
				}
				
				if (oApprovers.getModel("POItems")) {
				    oApprovers.getModel("POItems").setData();
				}
				
				if (oDelivered.getModel("Delivered")) {
				    oDelivered.getModel("Delivered").setData();
				}
				
				if (oScanned.getModel("InvoicedPO")) {
				    oScanned.getModel("InvoicedPO").setData();
				}

				if (oInvoiced.getModel("Invoiced")) {
				    oInvoiced.getModel("Invoiced").setData();
				}

				if (oPaid.getModel("Paid")) {
				    oPaid.getModel("Paid").setData();
				}
				
				this.getModel().metadataLoaded().then( function() {
					var sObjectPath = this.getModel().createKey("PurchaseReqHeaderSet", {
						Banfn :  sObjectId
					});
					this._bindView("/" + sObjectPath);
				}.bind(this));
			},

			/**
			 * Binds the view to the object path. Makes sure that detail view displays
			 * a busy indicator while data for the corresponding element binding is loaded.
			 * @function
			 * @param {string} sObjectPath path to the object to be bound to the view.
			 * @private
			 */
			_bindView : function (sObjectPath) {
				// Set busy indicator during view binding
				var oViewModel = this.getModel("detailView");

				// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
				oViewModel.setProperty("/busy", false);

				this.getView().bindElement({
					path : sObjectPath,
					events: {
						change : this._onBindingChange.bind(this),
						dataRequested : function () {
							oViewModel.setProperty("/busy", true);
						},
						dataReceived: function () {
							oViewModel.setProperty("/busy", false);
						}
					}
				});
			},

			_onBindingChange : function () {
				var oView = this.getView(),
					oElementBinding = oView.getElementBinding();

				// No data for the binding
				if (!oElementBinding.getBoundContext()) {
					this.getRouter().getTargets().display("detailObjectNotFound");
					// if object could not be found, the selection in the master list
					// does not make sense anymore.
					this.getOwnerComponent().oListSelector.clearMasterListSelection();
					return;
				}

				var sPath = oElementBinding.getPath(),
					oResourceBundle = this.getResourceBundle(),
					oObject = oView.getModel().getObject(sPath),
					sObjectId = oObject.Banfn,
					sObjectName = oObject.Ernam,
					oViewModel = this.getModel("detailView");
					
				if (oObject.Waers !== "") {
				    var oStatus = this.byId("objectStatus");
				    var sText = parseFloat(oObject.Preis).toFixed(2) + " " + oObject.Waers;
				    oStatus.setText(sText);
				}
				
				sap.ui.getCore().setModel(this.getView().getModel());
				sap.ui.getCore().setModel(sObjectId, "id");
				sap.ui.getCore().setModel(oObject.Fullname, "User");
				this._getPODetails(sObjectId);
				this.getOwnerComponent().oListSelector.selectAListItem(sPath);

				oViewModel.setProperty("/saveAsTileTitle",oResourceBundle.getText("shareSaveTileAppTitle", [sObjectName]));
				oViewModel.setProperty("/shareOnJamTitle", sObjectName);
				oViewModel.setProperty("/shareSendEmailSubject",
					oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
				oViewModel.setProperty("/shareSendEmailMessage",
					oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
			},

			_onMetadataLoaded : function () {
				// Store original busy indicator delay for the detail view
				var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
					oViewModel = this.getModel("detailView");

				// Make sure busy indicator is displayed immediately when
				// detail view is displayed for the first time
				oViewModel.setProperty("/delay", 0);
				oViewModel.setProperty("/lineItemTableDelay", 0);

				// Binding the view will set it to not busy - so the view is always busy if it is not bound
				oViewModel.setProperty("/busy", true);
				// Restore original busy indicator delay for the detail view
				oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
			},

            _getPODetails: function(sObjectId) {
                var aFilter = new Filter("Banfn", FilterOperator.EQ, sObjectId);
				var sServiceUrl = this.getView().getModel().sServiceUrl;
				var oModel = new ODataModel(sServiceUrl);
				var sPath = "/POItemsSet";
                var oIconTabBar = this.byId("iconTabBar");
                var oIconTab = this.byId("iconTabBarFilterPO");
                var that = this;
				
				oModel.read(sPath, {
					filters : [aFilter],
					urlParameters: {
                        "$expand": "POAccAssignment"
                    },
					async: false,
					success : function(oData, oResponse) {
						that.getModel("PODetails").setData(oData.results);
						var oTable = that.byId(Fragment.createId("fragmentPOItems", "poItemsList"));
						oTable.setModel(that.getModel("PODetails"), "POItems");
						oIconTab.setCount(oData.results.length);
						oIconTab.setIconColor(IconColor.Positive);
						oIconTab.setIconColor(IconColor.Positive);
						var oIconTabWFPR = that.byId("iconTabBarFilterWFPR");
						oIconTabWFPR.setIconColor(IconColor.Positive);
						
						oTable.bindItems({
							path : "POItems>/",
							template : oTable.getBindingInfo("items").template
						});
						
						var oJSONModel = new JSONModel();
						var sEbeln, aData = [];
						
						for (var i = 0; i < oData.results.length; i++) {
				            if (sEbeln !== oData.results[i].Ebeln) {
				                aData.push(oData.results[i]);
				                sEbeln = oData.results[i].Ebeln;
				            }
						}
						
						oJSONModel.setData(aData);
						var oApprovers = that.byId(Fragment.createId("fragmentPOApprovers", "POList"));
						oApprovers.setModel(oJSONModel, "POItems");
						
						oApprovers.bindItems({
							path : "POItems>/",
							template : oApprovers.getBindingInfo("items").template
						});
						
						that._getPOTrack(aData);
						that._getPOInvoiceDetails(aData);
					},
					error : function(oError) {
					    oIconTab.setCount(0);
						oIconTab.setIconColor(IconColor.Neutral);
						var oIconTabWFPR = that.byId("iconTabBarFilterWFPR");
						oIconTabWFPR.setIconColor(IconColor.Critical);
						var oIconTabPOApprovers = that.byId("iconTabBarFilterWFPO");
						oIconTabPOApprovers.setIconColor(IconColor.Neutral);
						var oIconTabDelivered = that.byId("iconTabBarFilterDelivered");
						oIconTabDelivered.setIconColor(IconColor.Neutral);
                        var oIconTabScanned = that.byId("iconTabBarFilterScanned");
                        oIconTabScanned.setIconColor(IconColor.Neutral);
						var oIconTabInvoiced = that.byId("iconTabBarFilterInvoiced");
						oIconTabInvoiced.setIconColor(IconColor.Neutral);
						var oIconTabPaid = that.byId("iconTabBarFilterPaid");
						oIconTabPaid.setIconColor(IconColor.Neutral);
						oIconTabBar.setSelectedKey("WFPR");
					}
				});
            },
            
            _getPOInvoiceDetails: function(aOrders) {
				var sServiceUrl = this.getView().getModel().sServiceUrl;
				var oModel = new ODataModel(sServiceUrl);
				var sPath;
				this._aInvoicedOrders = [];
				this._iLength = aOrders.length;
				var that = this;
				
				
				for (var i = 0; i < aOrders.length; i++) {
				    sPath = "/POInvoiceSet('" + aOrders[i].Ebeln + "')";
				    
				    oModel.read(sPath, {
    					async: false,
    					success : function(oData, oResponse) {
    					    that._aInvoicedOrders.push(oData);
    					    
    					    if (that._iLength === that._aInvoicedOrders.length) {
    					        for (var j = 0; j < that._aInvoicedOrders.length; j++) {
    					            if (that._aInvoicedOrders[j].InvDate) {
    					                that._aInvoicedOrders[j].InvDate = that._formatLongDate(that._aInvoicedOrders[j].InvDate);
    					                var oIconTab = that.byId("iconTabBarFilterScanned");
        						        oIconTab.setIconColor(sap.ui.core.IconColor.Positive);
    					            }
    					        }
    					        
        					    var oJSONModel = new JSONModel();
                                oJSONModel.setData(that._aInvoicedOrders);
        						var oInvoicedOrders = that.byId(Fragment.createId("fragmentInvoicedPO", "InvoicedPOList"));
        						oInvoicedOrders.setModel(oJSONModel, "InvoicedPO");
        						
        						oInvoicedOrders.bindItems({
        							path : "InvoicedPO>/",
        							template : oInvoicedOrders.getBindingInfo("items").template
        						});
    					    }
    					},
    					error : function(oError) {
    					    var oIconTab = that.byId("iconTabBarFilterPO");
    					    oIconTab.setCount(0);
    						oIconTab.setIconColor(sap.ui.core.IconColor.Neutral);
    						oIconTab = that.byId("iconTabBarFilterWFPR");
    						oIconTab.setIconColor(sap.ui.core.IconColor.Neutral);
    					}
    				});
				}
            },
            
            _getPOTrack: function(aData) {
                this._aOrdered = [];
                this._aInvoiced = [];
                this._aPaid = [];
                this._aDelivered = [];
                this._iLength = aData.length;
                bDeliveredChecked = false;
                bInvoicedChecked = false;
                bPaidChecked = false;
                bDelivered = false;
                bInvoiced = false;
                bPaid = false;
				var sServiceUrl = this.getView().getModel("trackPO").sServiceUrl;
				var oIconTab, sPath, sEbeln;
				var that = this;
				var oModel = new ODataModel(sServiceUrl);
				
				for (var j = 0; j < aData.length; j++) {
				    sPath = "/PODetailedDatas(PONumber='" + aData[j].Ebeln + "')";
				    
				    oModel.read(sPath, {
    				    urlParameters: {
                            "$expand": "POItems"
                        },
                        async: false,
						success : function(oData, oResponse) {
							for (var i = 0; i < oData.POItems.results.length; i++) {
								var mDelivered = {}, mInvoiced = {}, mPaid = {};
	                            mInvoiced.PONumber = oData.POItems.results[i].PONumber;
	                            mInvoiced.ItemNumber = oData.POItems.results[i].ItemNumber;
	                            mInvoiced.ItemNumberFormatted = oData.POItems.results[i].ItemNumberFormatted;
	                            mInvoiced.Status = oData.POItems.results[i].Status;
	                            mInvoiced.Description = oData.POItems.results[i].Description;
	                            mInvoiced.QuantityInvoiced = oData.POItems.results[i].QuantityInvoiced;
	                            mInvoiced.UnitInvoiced = oData.POItems.results[i].UnitInvoiced;
	                            mInvoiced.ValueInvoiced = oData.POItems.results[i].ValueInvoiced;
	                            mInvoiced.CurrencyInvoiced = oData.POItems.results[i].CurrencyInvoiced;
	                            mInvoiced.InvoicedDate = oData.POItems.results[i].InvoicedDate;
	                            that._aInvoiced.push(mInvoiced);
	
	                            if (sEbeln !== oData.POItems.results[i].PONumber) {
	                                mPaid.PONumber = oData.POItems.results[i].PONumber;
	                                mPaid.ItemNumber = oData.POItems.results[i].ItemNumber;
	                                mPaid.ItemNumberFormatted = oData.POItems.results[i].ItemNumberFormatted;
	                                mPaid.MaterialID = oData.POItems.results[i].MaterialID;
	                                mPaid.Description = oData.POItems.results[i].Description;
	                                mPaid.SplCondPaid = oData.POItems.results[i].SplCondPaid;
	                                mPaid.SplCondPaidMessage = oData.POItems.results[i].SplCondPaidMessage;
	                                mPaid.ValuePaid = oData.POItems.results[i].ValuePaid;
	                                mPaid.CurrencyPaid = oData.POItems.results[i].CurrencyPaid;
	                                that._aPaid.push(mPaid);
	                                sEbeln = oData.POItems.results[i].PONumber;
	                                
	                                if (mPaid.SplCondPaid === "3") {
	                                    bPaid = true;
	                                }
	                        	}
	                        
	                            mDelivered.PONumber = oData.POItems.results[i].PONumber;
	                            mDelivered.ItemNumber = oData.POItems.results[i].ItemNumber;
	                            mDelivered.ItemNumberFormatted = oData.POItems.results[i].ItemNumberFormatted;
	                            mDelivered.Status = oData.POItems.results[i].Status;
	                            mDelivered.Description = oData.POItems.results[i].Description;
	                            mDelivered.QuantityDelivered = oData.POItems.results[i].QuantityDelivered;
	                            mDelivered.UnitDelivered = oData.POItems.results[i].UnitDelivered;
	                            mDelivered.ValueDelivered = oData.POItems.results[i].ValueDelivered;
	                            mDelivered.Currency = oData.POItems.results[i].Currency;
	                            mDelivered.DeliveredDate = oData.POItems.results[i].DeliveredDate;
	                            that._aDelivered.push(mDelivered);
					    	}
					    
							that._setIconStatus(oData);
					    
						    if (that._iLength === that._aPaid.length) {
	                            var oInvoicedModel = new JSONModel();
	                            var oPaidModel = new JSONModel("paid");
	                            var oDeliveredModel = new JSONModel("delivered");
	                            
	                            oInvoicedModel.setData(that._aInvoiced);
	                            var oInvoicedTable = that.byId(Fragment.createId("invoicedPOItems", "poStatusInvoiced"));
	                            oInvoicedTable.setModel(oInvoicedModel, "Invoiced");
	                            
	                            oInvoicedTable.bindItems({
	                            	path : "Invoiced>/",
	                            	template : oInvoicedTable.getBindingInfo("items").template
	                            });
	                            
	                            oPaidModel.setData(that._aPaid);
	                            var oPaidTable = that.byId(Fragment.createId("paidPOItems", "poStatusPaid"));
	                            oPaidTable.setModel(oPaidModel, "Paid");
	                            
	                            oPaidTable.bindItems({
	                            	path : "Paid>/",
	                            	template : oPaidTable.getBindingInfo("items").template
	                            });
	                            
	                            oDeliveredModel.setData(that._aDelivered);
	                            var oDeliveredTable = that.byId(Fragment.createId("deliveredPOItems", "poStatusDelivered"));
	                            oDeliveredTable.setModel(oDeliveredModel, "Delivered");
	                        
	                            oDeliveredTable.bindItems({
	                            	path : "Delivered>/",
	                            	template : oDeliveredTable.getBindingInfo("items").template
	                            });
	                            
	                            var oIconTabDelivered = that.byId("iconTabBarFilterDelivered");
	                            var oIconTabScanned = that.byId("iconTabBarFilterScanned");
	    						var oIconTabInvoiced = that.byId("iconTabBarFilterInvoiced");
	    						var oIconTabPaid = that.byId("iconTabBarFilterPaid");
	    						var oIconTabBar = that.byId("iconTabBar");
	    						var oIconTabWFPO = that.byId("iconTabBarFilterWFPO");
	    						
	    						if (bPaidChecked) {
	                                oIconTabPaid.setIconColor(IconColor.Neutral);
	                                
	                                if (!bInvoicedChecked) {
	                                    if (!sap.ui.getCore().getModel("selectedTab")) {
	                                        oIconTabBar.setSelectedKey("Paid");
	                                        sap.ui.getCore().setModel(true, "selectedTab");
	                                    }
	                                    
	                                    oIconTabPaid.setIconColor(IconColor.Critical);
	                                }
	                            } else {
	                                oIconTabPaid.setIconColor(IconColor.Critical);
	                                
	                                if (!sap.ui.getCore().getModel("selectedTab")) {
	                                    oIconTabBar.setSelectedKey("Paid");
	                                    sap.ui.getCore().setModel(true, "selectedTab");
	                                }
	                            }
	                        
	                            if (bPaid) {
	                                oIconTabPaid.setIconColor(IconColor.Critical);
	                                
	                                if (!sap.ui.getCore().getModel("selectedTab")) {
	                                    oIconTabBar.setSelectedKey("Paid");
	                                    sap.ui.getCore().setModel(true, "selectedTab");
	                                }
	                            }
	                            
	                            if (!bDeliveredChecked) {
	                                oIconTabDelivered.setIconColor(IconColor.Positive);
	                            } 
	                        
	                            if (bInvoicedChecked) {
	                                oIconTabInvoiced.setIconColor(IconColor.Neutral);
	                                oIconTabScanned.setIconColor(IconColor.Neutral);
	                                
	                                if (!bDeliveredChecked) {
	                                    oIconTabScanned.setIconColor(IconColor.Positive);
	                                    oIconTabInvoiced.setIconColor(IconColor.Critical);
	                                    
	                                    if (!sap.ui.getCore().getModel("selectedTab")) {
	                                        oIconTabBar.setSelectedKey("Invoiced");
	                                        sap.ui.getCore().setModel(true, "selectedTab");
	                                    }
	                                }
	                            } else {
	                                oIconTabInvoiced.setIconColor(IconColor.Positive);
	                                oIconTabScanned.setIconColor(IconColor.Positive);
	                            }
					
	                            if (oData.NumberOfItemsDelivered === 0) {
	                                oIconTabWFPO.setIconColor(IconColor.Critical);
	                                oIconTabDelivered.setIconColor(IconColor.Neutral);
	                                
	                                if (!sap.ui.getCore().getModel("selectedTab")) {
	                                    oIconTabBar.setSelectedKey("WFPO");
	                                    sap.ui.getCore().setModel(true, "selectedTab");
	                                }
	                            } else {
	                                oIconTabWFPO.setIconColor(IconColor.Positive);
	                                
	                                if (!bPaid) {
	                                    if (bDeliveredChecked) {
	                                        oIconTabDelivered.setIconColor(IconColor.Critical);
	                                    }
	                                }
	                                
	                                if (!sap.ui.getCore().getModel("selectedTab")) {
	                                    oIconTabBar.setSelectedKey("Delivered");
	                                    sap.ui.getCore().setModel(true, "selectedTab");
	                                }
	                            }
					    	}
						},
						error : function (oError) {
							oIconTab = that.byId("iconTabBarFilterWFPO");
							oIconTab.setIconColor(sap.ui.core.IconColor.Neutral);
						}
			        });
				}

                // if (this._aPaid.length === 0) {
                //     oIconTab = that.byId("iconTabBarFilterPaid");
                //     oIconTab.setIconColor(sap.ui.core.IconColor.Neutral);
                // } else {
                //     oIconTab = that.byId("iconTabBarFilterPaid");
                //     oIconTab.setIconColor(sap.ui.core.IconColor.Positive);
                // }
            },
            
            _formatDate : function(sValue) {
    		 	var dValue = new Date(sValue);
    		 	
    		    var dFormat = DateFormat.getDateInstance({
    		    	pattern: "dd/MM/yyyy"
    	    	});
    		    var dFormatted = dFormat.format(dValue);
    		    return dFormatted;
    		 },
    		 
    		 _setIconStatus: function(aData) {
                if (!bDeliveredChecked) {
                    if (aData.NumberOfItemsDelivered === aData.NumberOfItemsInvoiced) {
                        if (aData.NumberOfItemsDelivered === 0) {
                            bDeliveredChecked = true;
                        } else {
                            bDelivered = true;
                        }
                    } else {
                        bDelivered = false; 
                        bDeliveredChecked = true;
                    }
                }
                
                if (!bInvoicedChecked) {
                    if (aData.NumberOfItemsInvoiced === aData.NumberOfItemsPaid) {
                        if (aData.NumberOfItemsInvoiced === 0) {
                            bInvoicedChecked = true;
                        } else {
                            bInvoiced = true;
                        }
                    } else {
                        bInvoiced = false;
                        bInvoicedChecked = true;
                    }
                }
                
                if (!bPaidChecked) {
                    if (aData.NumberOfItemsDelivered === aData.NumberOfItemsPaid) {
                        if (aData.NumberOfItemsInvoiced === 0) {
                            bPaidChecked = true;
                        } else {
                            bPaid = true;
                        }
                    } else {
                        bPaid = false;
                        bPaidChecked = true;
                    }
                }
            },
            
            _formatLongDate: function(sValue) {
			 	var dValue = new Date(sValue);
			    var sTime = dValue.getTime();
			    
			    if (isNaN(sTime)) {
					return false;
			    }
			    var dFormat = DateFormat.getDateInstance({
			    	pattern: "dd/MM/yyyy"
		    	});
			    var sTimeOffset = new Date(0).getTimezoneOffset() * 60 * 1000;
			    return dFormat.format(new Date(sTime + sTimeOffset));
			 }
		});
	}
);