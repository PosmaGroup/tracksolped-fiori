sap.ui.define([
		"com/bac/trackpr/model/GroupSortState",
		"sap/ui/model/json/JSONModel"
	], function (GroupSortState, JSONModel) {
	"use strict";

	QUnit.module("GroupSortState - grouping and sorting", {
		beforeEach: function () {
			this.oModel = new JSONModel({});
			// System under test
			this.oGroupSortState = new GroupSortState(this.oModel, function() {});
		}
	});

	QUnit.test("Should always return a sorter when sorting", function (assert) {
		// Act + Assert
		assert.strictEqual(this.oGroupSortState.sort("Preis").length, 1, "The sorting by Preis returned a sorter");
		assert.strictEqual(this.oGroupSortState.sort("Ernam").length, 1, "The sorting by Ernam returned a sorter");
	});

	QUnit.test("Should return a grouper when grouping", function (assert) {
		// Act + Assert
		assert.strictEqual(this.oGroupSortState.group("Preis").length, 1, "The group by Preis returned a sorter");
		assert.strictEqual(this.oGroupSortState.group("None").length, 0, "The sorting by None returned no sorter");
	});


	QUnit.test("Should set the sorting to Preis if the user groupes by Preis", function (assert) {
		// Act + Assert
		this.oGroupSortState.group("Preis");
		assert.strictEqual(this.oModel.getProperty("/sortBy"), "Preis", "The sorting is the same as the grouping");
	});

	QUnit.test("Should set the grouping to None if the user sorts by Ernam and there was a grouping before", function (assert) {
		// Arrange
		this.oModel.setProperty("/groupBy", "Preis");

		this.oGroupSortState.sort("Ernam");

		// Assert
		assert.strictEqual(this.oModel.getProperty("/groupBy"), "None", "The grouping got reset");
	});
});