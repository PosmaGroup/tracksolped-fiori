jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/bac/trackpr/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/bac/trackpr/test/integration/pages/App",
	"com/bac/trackpr/test/integration/pages/Browser",
	"com/bac/trackpr/test/integration/pages/Master",
	"com/bac/trackpr/test/integration/pages/Detail",
	"com/bac/trackpr/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.bac.trackpr.view."
	});

	sap.ui.require([
		"com/bac/trackpr/test/integration/NavigationJourneyPhone",
		"com/bac/trackpr/test/integration/NotFoundJourneyPhone",
		"com/bac/trackpr/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});