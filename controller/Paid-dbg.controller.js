/*global location */
sap.ui.define([
		"com/bac/trackpr/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"sap/ui/model/odata/v2/ODataModel"
	], function (BaseController, JSONModel, Filter, FilterOperator, oDataModel) {
		"use strict";

		return BaseController.extend("com.bac.trackpr.controller.Paid", {
		    onInit: function() {
		        var oModel = new JSONModel();
		        this.getView().setModel(oModel);
                this.getRouter().getRoute("paid").attachPatternMatched(this._onObjectMatched, this);  
		    },
		    
		    onNavBack: function(oEvent) {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		        
                oRouter.navTo("object", {
                    objectId : sap.ui.getCore().getModel("id")
                }, true);   
		    },
			
			_onObjectMatched : function (oEvent) {
		        var oJSONModel = new JSONModel();
				oJSONModel.setData(sap.ui.getCore().getModel("Paid").getData());
				var oTable = this.byId("paidList");
				oTable.setModel(sap.ui.getCore().getModel("Paid"), "Paid");
				
			    oTable.bindItems({
					path : "Paid>/",
					template : oTable.getBindingInfo("items").template
				});
			}
        });
	}
);