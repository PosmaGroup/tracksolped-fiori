jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 PurchaseReqHeaderSet in the list
// * All 3 PurchaseReqHeaderSet have at least one ToItems

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/bac/trackpr/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/bac/trackpr/test/integration/pages/App",
	"com/bac/trackpr/test/integration/pages/Browser",
	"com/bac/trackpr/test/integration/pages/Master",
	"com/bac/trackpr/test/integration/pages/Detail",
	"com/bac/trackpr/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.bac.trackpr.view."
	});

	sap.ui.require([
		"com/bac/trackpr/test/integration/MasterJourney",
		"com/bac/trackpr/test/integration/NavigationJourney",
		"com/bac/trackpr/test/integration/NotFoundJourney",
		"com/bac/trackpr/test/integration/BusyJourney",
		"com/bac/trackpr/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});