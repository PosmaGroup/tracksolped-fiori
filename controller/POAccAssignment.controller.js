/*global location */
sap.ui.define([
		"com/bac/trackpr/controller/BaseController",
		"sap/ui/model/json/JSONModel"
	], function (BaseController, JSONModel) {
		"use strict";

		return BaseController.extend("com.bac.trackpr.controller.POAccAssignment", {
		    onInit: function() {
		        var oModel = new JSONModel();
		        this.getView().setModel(oModel);
                this.getRouter().getRoute("poAccAssignment").attachPatternMatched(this._onObjectMatched, this);  
		    },
		    
		    onNavBack: function(oEvent) {
                this.getRouter().navTo("object", {
                    objectId : sap.ui.getCore().getModel("id")
                }, true);   
		    },
			
			_onObjectMatched : function (oEvent) {
			    var aAccAssignment = sap.ui.getCore().getModel("POAcc").getData();
		        var oJSONModel = new JSONModel();
				oJSONModel.setData(aAccAssignment);
				var oTable = this.byId("POAccAssignmentList");
				oTable.setModel(oJSONModel, "POAccAssignment");
				
			    oTable.bindItems({
					path : "POAccAssignment>/",
					template : oTable.getBindingInfo("items").template
				});
				
				this.closeBusyIndicator();
			}
        });
	}
);