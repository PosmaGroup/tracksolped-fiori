/*global location */
sap.ui.define([
		"com/bac/trackpr/controller/BaseController",
		"sap/ui/model/json/JSONModel"
	], function (BaseController, JSONModel) {
		"use strict";

		return BaseController.extend("com.bac.trackpr.controller.Invoiced", {
		    onInit: function() {
		        var oModel = new JSONModel();
		        this.getView().setModel(oModel);
                this.getRouter().getRoute("invoiced").attachPatternMatched(this._onObjectMatched, this);  
		    },
		    
		    onNavBack: function(oEvent) {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		        
                oRouter.navTo("object", {
                    objectId : sap.ui.getCore().getModel("id")
                }, true);   
		    },
			
			_onObjectMatched : function (oEvent) {
			    var aItems = sap.ui.getCore().getModel("invoiceDetails").getData();
		        var oJSONModel = new JSONModel();
				oJSONModel.setData(aItems);
				var oTable = this.byId("invoicedDetailsList");
				oTable.setModel(oJSONModel, "invoicedDetails");
				
			    oTable.bindItems({
					path : "invoicedDetails>/",
					template : oTable.getBindingInfo("items").template
				});
			}
        });
	}
);