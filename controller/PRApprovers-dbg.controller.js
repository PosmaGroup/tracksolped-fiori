/*global location */
sap.ui.define([
		"com/bac/trackpr/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/model/odata/v2/ODataModel",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"sap/suite/ui/commons/ProcessFlowLaneHeader",
		"sap/suite/ui/commons/ProcessFlowNode",
		"sap/suite/ui/commons/ProcessFlowNodeState",
		"sap/suite/ui/commons/ProcessFlowZoomLevel",
		"sap/ui/core/format/DateFormat"
	], function (BaseController, JSONModel, oDataModel, Filter, FilterOperator,  ProcessFlowLaneHeader, ProcessFlowNode, ProcessFlowNodeState, 
	            ProcessFlowZoomLevel, DateFormat) {
		"use strict";

		return BaseController.extend("com.bac.trackpr.controller.PRApprovers", {
		    onInit: function() {
		        var oModel = new JSONModel();
		        this.getView().setModel(oModel);
                this.getRouter().getRoute("prapprovers").attachPatternMatched(this._onObjectMatched, this);  
		    },
		    
		    onNavBack: function(oEvent) {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		        
                oRouter.navTo("object", {
                    objectId : sap.ui.getCore().getModel("id")
                }, true);   
		    },
			
			_onObjectMatched : function (oEvent) {
			    var sBanfn = oEvent.getParameter("arguments").id;
			    var sBnfpo = oEvent.getParameter("arguments").item;
			    var sServiceUrl = sap.ui.getCore().getModel().sServiceUrl;
				var oModel = new oDataModel(sServiceUrl);
				var sPath = "/PRWFApproversSet";
				var aFilters = [];
			    var oFilterBanfn = new Filter("Banfn", FilterOperator.EQ, sBanfn);
			    var oFilterBnfpo = new Filter("Bnfpo", FilterOperator.EQ, sBnfpo);
			    var that = this;
			    
			    var oFilter = new Filter({
			        filters: [oFilterBanfn, oFilterBnfpo], 
			        and:true
			    });
				
				oModel.read(sPath, {
					filters : [oFilter],
					success : function(oData, oResponse) {
                        that._setProcessFlow(oData.results);
					},
					error : function(oError) {
					}
				});
			},

    		_setProcessFlow: function(aApprovers) {
                var oProcessFlow = this.byId("processflow");
				oProcessFlow.destroyLanes();
				oProcessFlow.destroyNodes();
				var j = 0, oLane, oNode, sText, sState, sStateText, bFocused, bHighlighted;
					
				oLane = new ProcessFlowLaneHeader({
					laneId: 0,
					position: 0,
					iconSrc: "sap-icon://employee",
					text: this.getModel("i18n").getResourceBundle().getText("detailsProcessFlowStateInitiator")
				});
					
				oProcessFlow.insertLane(oLane);
				
				oNode = new ProcessFlowNode({
					laneId: 0,
					nodeId: 0 ,
					children: [ 1 ],
					focused: false,
					highlighted: false,
					title: sap.ui.getCore().getModel("User"),
					state: ProcessFlowNodeState.Neutral
				});	
				
				oProcessFlow.insertNode(oNode);
					
				for (var i = 0; i < aApprovers.length; i++) {
					var aChildren = [], iChildren;
					j = i+1;
					var sText = this.getModel("i18n").getResourceBundle().getText("detailsProcessFlowLaneText") + " " + j;
					
					oLane = new ProcessFlowLaneHeader({
						laneId: j,
						position: j,
						iconSrc: "sap-icon://employee",
						text: sText
					});
					
					oProcessFlow.insertLane(oLane);
					
					if (aApprovers[i].Approved === "A") {
                        bFocused = false;
						bHighlighted = true;
						sState = ProcessFlowNodeState.Positive;
						sStateText = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("detailsProcessFlowStateApproved");
					} else if (aApprovers[i].Approved === "R") {
                        bHighlighted = true;
                        bFocused = true;
                        sState = ProcessFlowNodeState.Negative;
                        sStateText = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("detailsProcessFlowStateRejected");
					} else {
                        if (aApprovers[i].ActualLevel) {
                            sState = ProcessFlowNodeState.Planned;
                        } else {
                            sState = ProcessFlowNodeState.Neutral;
                        }
                        
                        bFocused = false;
						bHighlighted = false;
						sStateText = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("detailsProcessFlowStatePlanned");
					}
					
					if (i === aApprovers.length-1) {
						aChildren = [ ];
					} else {
					    iChildren = j+1;
						aChildren.push(iChildren);		
					}
					
					var dApproved = this._formatDate(aApprovers[i].WiAed);
					
					oNode = new ProcessFlowNode({
						laneId: j,
						nodeId: j,
						children: aChildren,
						focused: bFocused,
						highlighted: bHighlighted,
						title: aApprovers[i].WiAaName,
						state: sState,
						texts: dApproved,
						stateText: sStateText
					});	
				
					oProcessFlow.insertNode(oNode);
				}
				
				oProcessFlow.setZoomLevel(ProcessFlowZoomLevel.Two);
				
				if (oProcessFlow.getAggregation("nodes").length !== 0) {
				// 	oProcessFlow.updateModel().bind(this);
				}
            },
            
            _formatDate : function(sValue) {
    		 	var dValue = new Date(sValue);
    		 	var dFormatted;
    		 	
    		    var dFormat = DateFormat.getDateInstance({
    		    	pattern: "dd/MM/yyyy"
    	    	});
    		    var sTimeOffset = new Date(0).getTimezoneOffset() * 60 * 1000;
    		    
    		    if (sTimeOffset >= 0) {
    		        dFormatted = dFormat.format(new Date(sTimeOffset));
    		    } else {
    		        dFormatted = dFormat.format(dValue);
    		    }
    
			    return dFormatted;
    		 }
        });
	}
);