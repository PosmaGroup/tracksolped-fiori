/*global location */
sap.ui.define([
		"com/bac/trackpr/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/model/odata/v2/ODataModel",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"sap/suite/ui/commons/ProcessFlowLaneHeader",
		"sap/suite/ui/commons/ProcessFlowNode",
		"sap/suite/ui/commons/ProcessFlowNodeState",
		"sap/suite/ui/commons/ProcessFlowZoomLevel",
		"sap/ui/core/format/DateFormat",
		"sap/m/MessageToast"
	], function (BaseController, JSONModel, ODataModel, Filter, FilterOperator, ProcessFlowLaneHeader, ProcessFlowNode, ProcessFlowNodeState, 
	            ProcessFlowZoomLevel, DateFormat, MessageToast) {
		"use strict";

		return BaseController.extend("com.bac.trackpr.controller.POApprovers", {
		    onInit: function() {
		        var oModel = new JSONModel();
		        this.getView().setModel(oModel);
                this.getRouter().getRoute("poapprovers").attachPatternMatched(this._onObjectMatched, this);
		    },
		    
		    onNavBack: function(oEvent) {
                this.getRouter().navTo("object", {
                    objectId : sap.ui.getCore().getModel("id")
                }, true);   
		    },
			
			_onObjectMatched : function (oEvent) {
			    var sObjectId =  oEvent.getParameter("arguments").id;
			    var aFilter = new Filter("Ebeln", FilterOperator.EQ, sObjectId);
			    var sServiceUrl = sap.ui.getCore().getModel().sServiceUrl;
				var oModel = new ODataModel(sServiceUrl);
				var sPath = "/POWFApproversSet";
				var that = this;
				
				var oPromise = new Promise( function(resolve, reject) {
					oModel.read(sPath, {
						filters : [aFilter],
						success : function(oData, oResponse) {
							resolve(oData);
						},
						error : function(oError) {
							reject(oError);
						}
					});
				});
				
				oPromise
					.then( function (oData) {
						if (oData.results.length === 0) {
							MessageToast.show(that.getResourceBundle().getText("noPOApproversFound"));
						} else {
							that._setProcessFlow(oData.results);
						}
					})
					.finally( function (oError) {
						that.closeBusyIndicator();
					});
			},
			
			_setProcessFlow: function(aApprovers) {
                var oProcessFlow = this.byId("processflow");
				oProcessFlow.destroyLanes();
				oProcessFlow.destroyNodes();
				oProcessFlow.removeAllNodes();
				oProcessFlow.removeAllLanes();
				var j = 0, oLane, oNode, sText, sState, sStateText, bFocused, bHighlighted, dApproved;
					
				oLane = new ProcessFlowLaneHeader({
					laneId: 0,
					position: 0,
					iconSrc: "sap-icon://employee",
					text: this.getModel("i18n").getResourceBundle().getText("detailsProcessFlowStateInitiator")
				});
					
				oProcessFlow.insertLane(oLane);
				
				oNode = new ProcessFlowNode({
					laneId: 0,
					nodeId: 0 ,
					children: [ 1 ],
					focused: false,
					highlighted: false,
					title: sap.ui.getCore().getModel("User"),
					state: ProcessFlowNodeState.Neutral
				});	
				
				oProcessFlow.insertNode(oNode);
					
				for (var i = 0; i < aApprovers.length; i++) {
					var aChildren = [], iChildren;
					j = i + 1;
					sText = this.getModel("i18n").getResourceBundle().getText("detailsProcessFlowLaneText") + " " + j;
					
					oLane = new ProcessFlowLaneHeader({
						laneId: j,
						position: j,
						iconSrc: "sap-icon://employee",
						text: sText
					});
					
					oProcessFlow.insertLane(oLane);
					
					if (aApprovers[i].WiAed === null) {
						dApproved = "";
					} else {
						dApproved = this._formatDate(aApprovers[i].WiAed, aApprovers[i].WiAet.ms);
					}
					
					if (aApprovers[i].Approved === "A") {
                        bFocused = false;
						bHighlighted = true;
						sState = ProcessFlowNodeState.Positive;
						sStateText = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("detailsProcessFlowStateApproved");
					} else if (aApprovers[i].Approved === "R") {
                        bHighlighted = true;
                        bFocused = true;
                        sState = ProcessFlowNodeState.Negative;
                        sStateText = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("detailsProcessFlowStateRejected");
					} else {
					    dApproved = "";
					    sState = ProcessFlowNodeState.Neutral;
                        bFocused = false;
						bHighlighted = false;
						sStateText = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("detailsProcessFlowStatePlanned");
					}
					
					if (i === aApprovers.length - 1) {
						aChildren = [ ];
					} else {
					    iChildren = j + 1;
						aChildren.push(iChildren);	
					}

					oNode = new ProcessFlowNode({
						laneId: j,
						nodeId: j,
						children: aChildren,
						focused: bFocused,
						highlighted: bHighlighted,
						title: aApprovers[i].WiAaName,
						state: sState,
						texts: dApproved,
						stateText: sStateText
					});	
					
					oProcessFlow.insertNode(oNode);
				}
				
				oProcessFlow.setZoomLevel(ProcessFlowZoomLevel.Two);
				
				if (oProcessFlow.getAggregation("nodes").length !== 0) {
					oProcessFlow.updateModel().bind(this);
				}
            },
            
            _formatDate : function(sValue, sTime) {
    		 	var dValue = new Date(sValue);
    		 	var dFormatted;
    	    	var dTime = dValue.getTime();
    	    	dValue.setUTCMilliseconds(sTime);
			    
			    if (isNaN(dTime)) {
					return false;
			    }
			    
			    var dFormat = DateFormat.getDateTimeInstance({
			    	pattern: "dd/MM/yyyy",
			    	UTC: true,
			    	strictParsing: true
		    	});
    	    	
    	    	dFormatted = new Date(Date.UTC(dValue.getFullYear(), dValue.getMonth(), dValue.getDate(), 
    	    	                                dValue.getUTCHours(), dValue.getUTCMinutes(), dValue.getUTCSeconds()));
    	    	dFormatted = dFormat.format(dFormatted);
			    return dFormatted;
    		 }
        });
	}
);