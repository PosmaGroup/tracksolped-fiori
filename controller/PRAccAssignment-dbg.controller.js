/*global location */
sap.ui.define([
		"com/bac/trackpr/controller/BaseController",
		"sap/ui/model/json/JSONModel"
	], function (BaseController, JSONModel) {
		"use strict";

		return BaseController.extend("com.bac.trackpr.controller.PRAccAssignment", {
		    onInit: function() {
		        var oModel = new JSONModel();
		        this.getView().setModel(oModel);
                this.getRouter().getRoute("prAccAssignment").attachPatternMatched(this._onObjectMatched, this);  
		    },
		    
		    onNavBack: function(oEvent) {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		        
                oRouter.navTo("object", {
                    objectId : sap.ui.getCore().getModel("id")
                }, true);   
		    },
			
			_onObjectMatched : function (oEvent) {
			    var aAccAssignment = sap.ui.getCore().getModel("PRAcc").getData();
		        var oJSONModel = new JSONModel();
				oJSONModel.setData(aAccAssignment);
				var oTable = this.byId("PRAccAssignmentList");
				oTable.setModel(oJSONModel, "PRAccAssignment");
				
			    oTable.bindItems({
					path : "PRAccAssignment>/",
					template : oTable.getBindingInfo("items").template
				});
			}
        });
	}
);