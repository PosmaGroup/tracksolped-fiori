/*global location */
sap.ui.define([
		"com/bac/trackpr/controller/BaseController",
		"sap/ui/model/json/JSONModel"
	], function (BaseController, JSONModel) {
		"use strict";

		return BaseController.extend("com.bac.trackpr.controller.Delivered", {
		    onInit: function() {
		        var oModel = new JSONModel();
		        this.getView().setModel(oModel);
                this.getRouter().getRoute("delivered").attachPatternMatched(this._onObjectMatched, this);  
		    },
		    
		    onNavBack: function(oEvent) {
		        this.getRouter().navTo("object", {
                    objectId : sap.ui.getCore().getModel("id")
                }, true);   
		    },
			
			_onObjectMatched : function (oEvent) {
			    var aItems = sap.ui.getCore().getModel("deliveredDetails").getData();
		        var oJSONModel = new JSONModel();
				oJSONModel.setData(aItems);
				var oTable = this.byId("DeliveredList");
				oTable.setModel(oJSONModel, "deliveredDetails");
				
			    oTable.bindItems({
					path : "deliveredDetails>/",
					template : oTable.getBindingInfo("items").template
				});
				
				this.closeBusyIndicator();
			}
        });
	}
);